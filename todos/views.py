from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from django.forms import ModelForm
from todos.forms import TodoListForm, TodoItemForm
from django.urls import reverse_lazy


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list": todo_list}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list_detail = TodoList.objects.get(id=id)
    context = {"todo_list_detail": todo_list_detail}
    return render(request, "todos/details.html", context)


# Create your views here.


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.id)
    else:
        form = TodoListForm
    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=model_instance)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)
    else:
        form = TodoListForm(instance=model_instance)
    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.list.id)
    else:
        form = TodoItemForm
    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def todo_item_update(request, id):
    model_instance = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=model_instance)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.list.id)
    else:
        form = TodoItemForm(instance=model_instance)
    context = {
        "form": form,
    }
    return render(request, "todos/edit_item.html", context)
